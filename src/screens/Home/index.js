import React, {useState, useEffect} from 'react';
import {View, FlatList, ActivityIndicator} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

// Actions
import {getAllGenresAction} from '../../appState/genres/actions';
import {getAllPopularMoviesAction} from '../../appState/movies/actions';

// Components
import Header from '../../components/Header';
import RowCard from '../../components/RowCard';
import ColumnCard from '../../components/ColumnCard';
import Search from '../../components/Search';

// Styles
import styles from './styles';

const Index = () => {
  const [isGrid, setIsGrid] = useState(false);
  const dispatch = useDispatch();
  const {popularMovies, popularMoviesError, popularMoviesLoading} = useSelector(
    (state) => state.movies,
  );

  useEffect(() => {
    dispatch(getAllPopularMoviesAction());
    dispatch(getAllGenresAction());
  }, [dispatch]);

  const renderHeader = () => {
    return (
      <>
        <Header isGrid={isGrid} setIsGrid={setIsGrid} />
      </>
    );
  };
  return (
    <View style={styles.container}>
      <Search />

      <View>
        {popularMoviesLoading ? (
          <ActivityIndicator />
        ) : (
          <FlatList
            renderItem={({item}) =>
              isGrid ? <ColumnCard item={item} /> : <RowCard item={item} />
            }
            data={popularMovies}
            ListEmptyComponent={() => <ActivityIndicator />}
            keyExtractor={(item) => item.id.toString()}
            key={isGrid}
            numColumns={isGrid ? 2 : null}
            ListHeaderComponent={renderHeader}
          />
        )}
      </View>
    </View>
  );
};

export default Index;
