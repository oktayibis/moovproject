import React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Screens
import Home from '../screens/Home';
import MovieDetail from '../screens/MovieDetail';
import Settings from '../screens/Settings';

//  Styles
import {Colors} from '../styles/themeConstats';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Screens Navigators
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

// Select tabbar icons
const chooseTabBar = (routeName) => {
  switch (routeName) {
    case 'Home':
      return 'home-outline';
    case 'Settings':
      return 'person-outline';
    default:
      return 'home';
  }
};

const HomeScreens = () => (
  <Stack.Navigator>
    <Stack.Screen name="Home" component={Home} options={{title: 'Home'}} />
    <Stack.Screen
      name="MovieDetail"
      component={MovieDetail}
      options={{title: 'Movie Detail'}}
    />
  </Stack.Navigator>
);

const Router = () => {
  Ionicons.loadFont();
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({size, color}) => {
            return (
              <Ionicons
                name={chooseTabBar(route.name)}
                size={size}
                color={color}
              />
            );
          },
        })}
        tabBarOptions={{
          activeTintColor: Colors.red,
          inactiveTintColor: Colors.grey,
        }}>
        <Tab.Screen name="Home" component={HomeScreens} />
        <Tab.Screen name="Settings" component={Settings} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default Router;
