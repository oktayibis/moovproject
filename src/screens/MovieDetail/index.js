import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {View, ActivityIndicator, Alert, ScrollView} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {getMovieByIdAction} from '../../appState/movies/actions';
import {getActorsAction} from '../../appState/actors/actions';

// Styles
import {
  Image,
  RowContainer,
  Container,
  HeartContainer,
} from '../../styles/StackStyles';
import {LightText, SolidText} from '../../styles/TextStyles';
import FontAwsome from 'react-native-vector-icons/FontAwesome';

// Components
import Link from '../../components/Link';
import CastList from '../../components/CastList';

// Constants
import {URLs, utils} from '../../utils';

const Index = () => {
  const route = useRoute();
  const dispatch = useDispatch();
  const {movie, movieLoading, movieError} = useSelector(
    (state) => state.movies,
  );
  const {actorsLoading, actors, actorsError, crew} = useSelector(
    (state) => state.actors,
  );
  const {id, genres, isLiked} = route.params;

  // Hanlde this movie is user's fav list. We took initial value from card with route params
  const [isFav, setIsFav] = React.useState(isLiked);

  React.useEffect(() => {
    dispatch(getMovieByIdAction(id));
    dispatch(getActorsAction(id));
  }, []);

  return movieLoading || actorsLoading ? (
    <View>
      <ActivityIndicator />
    </View>
  ) : (
    <ScrollView>
      <View>
        <Image
          defaultSource={require('../../../assets/images/loading.gif')}
          source={{uri: URLs.BASE_IMG_URL + movie.poster_path}}>
          <HeartContainer onPress={() => setIsFav(!isFav)} isLiked={isFav}>
            <FontAwsome name="heart" size={40} color={'white'} />
          </HeartContainer>
        </Image>
      </View>
      <View style={{marginTop: 30}}>
        <RowContainer>
          <View>
            <SolidText>Duration</SolidText>
            <LightText>{utils.convertMinsToHour(movie.runtime)}</LightText>
          </View>
          <View>
            <SolidText>Genre</SolidText>
            <LightText>
              {genres.join('/').length > 20
                ? genres.join('/\n')
                : genres.join('/')}
            </LightText>
          </View>
          <View>
            <SolidText>Language</SolidText>
            <LightText>{utils.convertMinsToHour(movie.runtime)}</LightText>
          </View>
        </RowContainer>
        <Container>
          <SolidText>Synopsis</SolidText>
          <LightText>{movie.overview.slice(0, 140)}...</LightText>
          <Link label="Read More" />
        </Container>
        <CastList count={3} list={actors} title="Main cast" />
        <CastList count={3} list={crew} title="Main technical team" />
      </View>
      {(movieError || actorsError) &&
        Alert.alert('Error', movieError || actorsError)}
    </ScrollView>
  );
};

export default Index;
