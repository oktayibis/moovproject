export const URLs = {
  // All images starting with below url
  BASE_IMG_URL: 'http://image.tmdb.org/t/p/w1280/',

  // If any persone doesnt have any image on backend, placeholder url
  placeHolderPerson:
    'https://www.morrealeres.com/morreale/wp-content/uploads/2017/04/placeholder-person.jpg',
};

export const utils = {
  convertMinsToHour: (min) => {
    const hour = min / 60;
    const minutes = min % 60;

    return `${hour.toFixed(0)}h ${minutes}m`;
  },
};
