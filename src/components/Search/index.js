import React, {useState} from 'react';
import {useDispatch} from 'react-redux';

import {
  getMoviesBySearch,
  getAllPopularMoviesAction,
} from '../../appState/movies/actions';

// Styles
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {SearchContainer, SearchInput} from '../../styles/SearchStyles';
import {Colors} from '../../styles/themeConstats';

const Index = () => {
  FontAwesome.loadFont();
  const dispatch = useDispatch();
  const [searchText, setSearchText] = useState('');

  const handleSearch = () => {
    if (searchText) {
      dispatch(getMoviesBySearch(searchText));
    } else {
      dispatch(getAllPopularMoviesAction());
    }
  };

  return (
    <SearchContainer>
      <FontAwesome name="search" size={20} color={Colors.textColorSolid} />
      <SearchInput
        placeholder="Search"
        placeholderTextColor={Colors.textColorSolid}
        returnKeyType="search"
        onChangeText={(text) => setSearchText(text)}
        onEndEditing={handleSearch}
      />
    </SearchContainer>
  );
};

export default Index;
