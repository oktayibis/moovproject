import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {Title} from '../../styles/TextStyles';
import styles from './styles';

const Index = ({isGrid, setIsGrid}) => {
  MaterialCommunityIcons.loadFont();
  return (
    <View style={styles.container}>
      <Title>Most popular</Title>
      <TouchableOpacity onPress={() => setIsGrid(!isGrid)}>
        <MaterialCommunityIcons
          name={isGrid ? 'view-grid-outline' : 'view-agenda-outline'}
          size={24}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Index;
