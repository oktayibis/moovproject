import React from 'react';
import {View, Text} from 'react-native';
import {RowContainer, Container} from '../../styles/StackStyles';
import {SolidText} from '../../styles/TextStyles';
import {URLs} from '../../utils';
import ActorCard from '../../components/ActorCard';

const Index = ({title, list, count}) => {
  return (
    <View>
      <Container>
        <SolidText>{title}</SolidText>
      </Container>
      <RowContainer>
        {list.slice(0, count).map((item) => (
          <ActorCard
            key={item.profile_path + Math.random()}
            name={item.name}
            imageUrl={
              item.profile_path
                ? URLs.BASE_IMG_URL + item.profile_path
                : URLs.placeHolderPerson
            }
          />
        ))}
      </RowContainer>
    </View>
  );
};

export default Index;
