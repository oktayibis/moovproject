import React, {useEffect, useState} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

// Styles
import {CardImage, FavContainer} from '../../styles/RowCardStyles';
import {Title, LightText} from '../../styles/TextStyles';
import FontAwsome from 'react-native-vector-icons/FontAwesome';

import styles from './styles';

// All images starting with url below
import {URLs} from '../../utils';

const Index = ({item}) => {
  const [movieGenre, setMovieGenre] = useState([]);
  const [isLiked, setIsLiked] = useState(false);

  const {genres} = useSelector((state) => state.genres);
  const navigation = useNavigation();

  // Finding genres with their ids.
  const handleGenres = () => {
    genres &&
      genres.map((listGenre) => {
        return item.genre_ids.map((movieGenreID) => {
          if (movieGenreID === parseInt(listGenre.id)) {
            return setMovieGenre((movieGenre) => [
              ...movieGenre,
              listGenre.name,
            ]);
          } else {
            return null;
          }
        });
      });
  };

  useEffect(() => {
    handleGenres();
  }, [genres]);

  return (
    <TouchableOpacity
      onPress={() =>
        // We send basic infos to detail page via router
        navigation.navigate('MovieDetail', {
          id: item.id,
          genres: movieGenre,
          isLiked: isLiked,
        })
      }
      style={styles.container}>
      <CardImage
        defaultSource={require('../../../assets/images/loading.gif')}
        source={{uri: URLs.BASE_IMG_URL + item.backdrop_path}}>
        <FavContainer onPress={() => setIsLiked(!isLiked)}>
          <FontAwsome
            name={isLiked ? 'heart' : 'heart-o'}
            size={20}
            color={isLiked ? 'red' : 'white'}
          />
        </FavContainer>
      </CardImage>
      <View style={styles.content}>
        <View>
          <Title>{item.title}</Title>
          <LightText capitalize>
            {item.release_date.slice(0, 4)} | {item.original_language}
          </LightText>
          <LightText>
            {genres && movieGenre.join('/').length < 20
              ? movieGenre.join('/')
              : movieGenre.join('/').slice(0, 20) + '...'}
          </LightText>
        </View>
        <View>
          <LightText>{item.vote_average}</LightText>
          <LightText>Public</LightText>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Index;
