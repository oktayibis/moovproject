import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    marginVertical: 15,
    marginTop: 15,
    flexDirection: 'row',
  },
  content: {
    marginLeft: 20,
    justifyContent: 'space-between',
  },
});
