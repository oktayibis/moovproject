import React from 'react';
import {TouchableOpacity} from 'react-native';
import {LinkText} from '../../styles/StackStyles';

const Index = ({label, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <LinkText>{label}</LinkText>
    </TouchableOpacity>
  );
};

export default Index;
