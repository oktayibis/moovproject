import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import FontAwsome from 'react-native-vector-icons/FontAwesome';

import {
  CardContainer,
  CardImage,
  CardTitle,
  FavContainer,
} from '../../styles/ColumnCardStyles';

import {LightText} from '../../styles/TextStyles';

// All images starting with below url
import {URLs} from '../../utils';

const Index = ({item}) => {
  const [movieGenre, setMovieGenre] = useState([]);
  const [isLiked, setIsLiked] = useState(false);

  const {genres} = useSelector((state) => state.genres);
  const navigation = useNavigation();

  // Finding genres with their ids.
  const handleGenres = () => {
    genres &&
      genres.map((listGenre) => {
        return item.genre_ids.map((movieGenreID) => {
          if (movieGenreID === parseInt(listGenre.id)) {
            return setMovieGenre((genre) => [...genre, listGenre.name]);
          } else {
            return null;
          }
        });
      });
  };

  useEffect(() => {
    handleGenres();
  }, [genres]);

  return (
    <CardContainer
      onPress={() =>
        navigation.navigate('MovieDetail', {
          id: item.id,
          genres: movieGenre,
          isLiked,
        })
      }>
      <CardImage
        defaultSource={require('../../../assets/images/loading.gif')}
        source={{uri: URLs.BASE_IMG_URL + item.backdrop_path}}>
        <FavContainer onPress={() => setIsLiked(!isLiked)}>
          <FontAwsome
            name={isLiked ? 'heart' : 'heart-o'}
            size={20}
            color={isLiked ? 'red' : 'white'}
          />
        </FavContainer>
        <CardTitle>{item.title}</CardTitle>
      </CardImage>
      <View>
        <LightText>
          {item.release_date.slice(0, 4)} | {item.original_language}
        </LightText>
        <LightText>
          {genres && movieGenre.join('/').length < 20
            ? movieGenre.join('/')
            : movieGenre.join('/').slice(0, 20) + '...'}
        </LightText>
      </View>
    </CardContainer>
  );
};

export default Index;
