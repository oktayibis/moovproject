import React from 'react';
import {View} from 'react-native';
import {RoundedImage} from '../../styles/ActorsCardStyles';
import {LightText} from '../../styles/TextStyles';

const Index = ({name, imageUrl}) => {
  return (
    <View>
      <LightText>{name.slice(0, 8)}..</LightText>
      <RoundedImage
        defaultSource={require('../../../assets/images/loading.gif')}
        source={{uri: imageUrl}}
      />
    </View>
  );
};

export default Index;
