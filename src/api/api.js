import Axios from 'axios';

const defaultParams = {
  baseUrl: 'https://api.themoviedb.org/3',
  apiKey: '579c7fc7840ab037036071939351dc60',
  imgBaseURL: 'http://image.tmdb.org/t/p/w1280/',
};
// https://api.themoviedb.org/3//popular?api_key=579c7fc7840ab037036071939351dc60

const getAllPopularMovies = async () => {
  return await Axios.get(
    `${defaultParams.baseUrl}/movie/popular?api_key=${defaultParams.apiKey}`,
  );
};

const getMovie = async (movieId) => {
  return await Axios.get(
    `${defaultParams.baseUrl}/movie/${movieId}?api_key=${defaultParams.apiKey}`,
  );
};

const getGenres = async () => {
  return await Axios.get(
    `${defaultParams.baseUrl}/genre/movie/list?api_key=${defaultParams.apiKey}`,
  );
};

const searchByMovieName = async (text) => {
  return await Axios.get(
    `${defaultParams.baseUrl}/search/movie?api_key=${defaultParams.apiKey}&query=${text}`,
  );
};

const getCasts = async (movieID) => {
  return await Axios.get(
    `${defaultParams.baseUrl}/movie/${movieID}/credits?api_key=${defaultParams.apiKey}`,
  );
};
export default {
  getAllPopularMovies,
  getMovie,
  getGenres,
  searchByMovieName,
  getCasts,
};
