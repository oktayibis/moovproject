import {
  GET_MOVIE_FAIL,
  GET_MOVIE_START,
  GET_MOVIE_SUCCESS,
  GET_POPULAR_MOVIES_FAIL,
  GET_POPULAR_MOVIES_START,
  GET_POPULAR_MOVIES_SUCCESS,
} from './types';
import api from '../../api/api';
import {Alert} from 'react-native';

// It can use saga for middleware managment
export const getAllPopularMoviesAction = () => {
  return async (dispatch) => {
    dispatch({type: GET_POPULAR_MOVIES_START});
    try {
      const response = await api.getAllPopularMovies();
      dispatch({
        type: GET_POPULAR_MOVIES_SUCCESS,
        popularMovies: response.data.results,
      });
    } catch (error) {
      dispatch({type: GET_POPULAR_MOVIES_FAIL, error});
      Alert.alert('Error', `Please re-start application.\n${error}`);
    }
  };
};

export const getMovieByIdAction = (id) => {
  return async (dispatch) => {
    dispatch({type: GET_MOVIE_START});
    try {
      const response = await api.getMovie(id);
      dispatch({type: GET_MOVIE_SUCCESS, movie: response.data});
    } catch (error) {
      dispatch({type: GET_MOVIE_FAIL, error});
      Alert.alert('Error', `Please re-start application.\n${error}`);
    }
  };
};

export const getMoviesBySearch = (text) => {
  return async (dispatch) => {
    dispatch({type: GET_POPULAR_MOVIES_START});
    try {
      const response = await api.searchByMovieName(text);
      dispatch({
        type: GET_POPULAR_MOVIES_SUCCESS,
        popularMovies: response.data.results,
      });
    } catch (error) {
      dispatch({type: GET_POPULAR_MOVIES_FAIL, error});
      Alert.alert('Error', `Please re-start application.\n${error}`);
    }
  };
};
