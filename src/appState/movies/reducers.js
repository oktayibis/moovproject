import {getActionFromState} from '@react-navigation/native';
import {
  GET_MOVIE_FAIL,
  GET_MOVIE_START,
  GET_MOVIE_SUCCESS,
  GET_POPULAR_MOVIES_FAIL,
  GET_POPULAR_MOVIES_START,
  GET_POPULAR_MOVIES_SUCCESS,
} from './types';

const INITIAL_STATE = {
  popularMovies: null,
  movie: null,
  popularMoviesLoading: true,
  movieLoading: true,
  popularMoviesError: null,
  movieError: null,
  genres: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_POPULAR_MOVIES_START:
      return {
        ...state,
        popularMoviesLoading: true,
      };
    case GET_POPULAR_MOVIES_SUCCESS:
      return {
        ...state,
        popularMoviesLoading: false,
        error: action.error,
        popularMovies: action.popularMovies,
      };
    case GET_POPULAR_MOVIES_FAIL:
      return {
        ...state,
        popularMoviesLoading: true,
      };

    case GET_MOVIE_START:
      return {
        ...state,
        movieLoading: true,
      };
    case GET_MOVIE_SUCCESS:
      return {
        ...state,
        movieLoading: false,
        movie: action.movie,
      };
    case GET_MOVIE_FAIL:
      return {
        ...state,
        movieLoading: false,
        movieError: action.error,
      };

    default:
      return {...state};
  }
};
