import {GET_GENRES_FAIL, GET_GENRES_START, GET_GENRES_SUCCESS} from './types';

const INITIAL_STATE = {
  genresLoading: true,
  genres: null,
  genresError: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_GENRES_START:
      return {
        ...state,
        genresLoading: true,
      };

    case GET_GENRES_SUCCESS:
      return {
        ...state,
        genresLoading: false,
        genres: action.genres,
      };

    case GET_GENRES_FAIL:
      return {
        ...state,
        genresLoading: false,
        genresError: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};
