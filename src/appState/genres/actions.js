import {GET_GENRES_FAIL, GET_GENRES_START, GET_GENRES_SUCCESS} from './types';
import api from '../../api/api';

export const getAllGenresAction = () => {
  return async (dispatch) => {
    dispatch({type: GET_GENRES_START});
    try {
      const response = await api.getGenres();
      dispatch({type: GET_GENRES_SUCCESS, genres: response.data.genres});
    } catch (error) {
      dispatch({type: GET_GENRES_FAIL, error});
    }
  };
};
