import {GET_CAST_FAIL, GET_CAST_START, GET_CAST_SUCCESS} from './types';

const INITIAL_STATE = {
  actorsLoading: true,
  actors: null,
  actorsError: null,
  crew: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_CAST_START:
      return {
        ...state,
        actorsLoading: true,
      };

    case GET_CAST_SUCCESS:
      return {
        ...state,
        actorsLoading: false,
        actors: action.actors,
        crew: action.crew,
      };

    case GET_CAST_FAIL:
      return {
        ...state,
        actorsLoading: false,
        actorsError: action.error,
      };
    default:
      return {
        ...state,
      };
  }
};
