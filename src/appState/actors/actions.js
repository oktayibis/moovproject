import api from '../../api/api';
import {GET_CAST_START, GET_CAST_SUCCESS, GET_CAST_FAIL} from './types';

export const getActorsAction = (id) => {
  return async (dispatch) => {
    dispatch({type: GET_CAST_START});
    try {
      const response = await api.getCasts(id);
      dispatch({
        type: GET_CAST_SUCCESS,
        actors: response.data.cast,
        crew: response.data.crew,
      });
    } catch (error) {
      dispatch({type: GET_CAST_FAIL, error});
    }
  };
};
