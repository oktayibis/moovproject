import {combineReducers} from 'redux';
import MovieReducers from './movies/reducers';
import GenresReducers from './genres/reducers';
import ActorsReducers from './actors/reducers';
export default combineReducers({
  movies: MovieReducers,
  genres: GenresReducers,
  actors: ActorsReducers,
});
