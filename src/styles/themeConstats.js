export const Colors = {
  grey: '#A3A6AB',
  red: '#FF354A',
  textColorSolid: '#575757',
  textColorLight: '#9C9DA1',
  lightGray: '#e4e4e4',
};

export const Fonts = {
  regular: 'WorkSans-Regular',
  bold: 'WorkSans-Bold',
  medium: 'WorkSans-Regular',
  semiBold: 'WorkSans-Regular',
};
