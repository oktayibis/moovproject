import styled from 'styled-components/native';
import {Colors} from '../themeConstats';

export const Image = styled.ImageBackground`
  width: 100%;
  height: 300px;
`;

export const RowContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 5%;
`;

export const Container = styled.View`
  margin: 0 5%;
`;

export const LinkText = styled.Text`
  color: #ff5c6d;
  text-align: right;
`;

export const HeartContainer = styled.TouchableOpacity`
  width: 60px;
  height: 60px;
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => (props.isLiked ? Colors.red : Colors.grey)};
  position: absolute;
  bottom: -30px;
  right: 10px;
`;
