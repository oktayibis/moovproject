import styled from 'styled-components/native';
import {Fonts} from '../themeConstats';
export const CardContainer = styled.TouchableOpacity`
  margin: 10px;
`;

export const CardImage = styled.ImageBackground`
  width: 160px;
  height: 200px;
  justify-content: flex-end;
  border-radius: 10px;
  overflow: hidden;
`;

export const CardTitle = styled.Text`
  color: white;
  font-weight: 800;
  font-size: 18px;
  background-color: rgba(0, 0, 0, 0.2);
  padding: 2px;
  font-family: ${Fonts.semiBold};
`;

export const FavContainer = styled.TouchableOpacity`
  border-width: 2px;
  border-color: white;
  border-radius: 50px;
  justify-content: center;
  align-items: center;
  width: 33px;
  height: 33px;
  position: absolute;
  top: 0;
  right: 0;
  margin-top: 10px;
  margin-right: 10px;
`;
