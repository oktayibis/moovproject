import styled from 'styled-components/native';
import {Colors, Fonts} from './themeConstats';
export const SolidText = styled.Text`
  color: ${Colors.textColorSolid};
  font-size: 18px;
  font-weight: 600;
  font-family: ${Fonts.medium};
`;

export const Title = styled.Text`
  color: ${Colors.textColorSolid};
  font-size: 24px;
  margin-top: 5px;
  margin-bottom: 5px;
  font-family: ${Fonts.bold};
`;

export const LightText = styled.Text`
  color: ${Colors.textColorLight};
  font-weight: 500;
  font-size: 16px;
  margin-top: 5px;
  margin-bottom: 5px;
  text-transform: ${(props) => (props.capitalize ? 'capitalize' : 'none')};
  font-family: ${Fonts.regular};
`;
