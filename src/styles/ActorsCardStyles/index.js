import styled from 'styled-components/native';

export const RoundedImage = styled.Image`
  width: 61px;
  height: 61px;
  border-radius: 50px;
  align-self: center;
`;
