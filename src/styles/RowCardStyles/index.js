import styled from 'styled-components/native';
import {Colors} from '../themeConstats';

export const CardImage = styled.ImageBackground`
  width: 135px;
  height: 200px;
  border-radius: 10px;
  overflow: hidden;
`;

export const FavContainer = styled.TouchableOpacity`
  border-width: 2px;
  border-color: white;
  border-radius: 50px;
  justify-content: center;
  align-items: center;
  width: 33px;
  height: 33px;
  position: absolute;
  right: 0;
  margin-top: 10px;
  margin-right: 10px;
`;
