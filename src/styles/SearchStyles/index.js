import styled from 'styled-components/native';
import {Colors, Fonts} from '../themeConstats';

export const SearchInput = styled.TextInput`
  width: 70%;
  color: ${Colors.textColorSolid};
  align-items: center;
  padding: 5px;
  margin-left: 5px;
  font-family: ${Fonts.bold};
`;

export const SearchContainer = styled.TouchableOpacity`
  border-radius: 14px;
  background-color: ${Colors.lightGray};
  flex-direction: row;
  padding: 5px 10px;
  align-items: center;
  border-radius: 20px;
  margin-bottom: 10px;
`;
