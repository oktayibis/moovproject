import React from 'react';
import {SafeAreaView, Text, StatusBar, View} from 'react-native';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';
import Router from './src/screens/Router';
import AllReducers from './src/appState/';

const App: () => React$Node = () => {
  const store = createStore(AllReducers, {}, applyMiddleware(ReduxThunk));
  return (
    <Provider store={store}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        <Router />
      </SafeAreaView>
    </Provider>
  );
};

export default App;
